export memory e(initial: 256, max: 256);

global g_a:int = 5245712;

export table i:funcref(min: 4, max: 4);

data d_passedlengthcheck_4K1gpass3O(offset: 1024) =
  "passed length check\00_4K+1\1c\1e}g\00pass3 OK\00pass2 OK\00pass1 OK\00"
  "3W{FTC_HR'\00Login module loaded\0a";
data d_b(offset: 1120) = "\05";
data d_c(offset: 1132) = "\01";
data d_d(offset: 1156) =
"\02\00\00\00\03\00\00\00\18\05\00\00\00\04";
data d_e(offset: 1180) = "\01";
data d_f(offset: 1196) = "\ff\ff\ff\ff\0a";
data d_g(offset: 1264) = "\10\0bP";

import function a_a(a:int, b:int, c:int, d:int):int;

import function a_b(a:int):int;

import function a_c(a:int, b:int, c:int);

import function a_d(a:int, b:int, c:int);

function f_e(a:int):int {
  var c:int;
  var b:int = d_g[0]:int;
  a = b + (c = a + 7 & -8);
  if (select_if(c, 0, a <= b)) goto B_a;
  if (a > memory_size() << 16) { if (eqz(a_b(a))) goto B_a }
  d_g[0]:int = a;
  return b;
  label B_a:
  2328[0]:int = 48;
  return -1;
}

function f_f(a:int) {
  var b:byte_ptr;
  d_f[0]:int;
  if (select_if(-1,
                0,
                {
                  b = f_h(a);
                  if (b ==
                      (a = {
                             if (d_f[0]:int < 0) {
                               f_i(a, b);
                               goto B_c;
                             }
                             f_i(a, b);
                             label B_c:
                           })) goto B_b;
                  a;
                  label B_b:
                } !=
                b) <
      0) goto B_a;
  if (d_f[1]:int == 10) goto B_e;
  a = 1140[0]:int;
  if (a == 1136[0]:int) goto B_e;
  1140[0]:int = a + 1;
  a[0]:byte = 10;
  goto B_a;
  label B_e:
  a = g_a - 16;
  g_a = a;
  a[15]:byte = 10;
  b = 1136[0]:int;
  if (if (b) {
        b
      } else {
        if (f_j()) goto B_f;
        1136[0]:int;
      } ==
      (b = 1140[0]:int)) goto B_g;
  if (d_f[1]:int == 10) goto B_g;
  1140[0]:int = b + 1;
  b[0] = 10;
  goto B_f;
  label B_g:
  if (call_indirect(1120, a + 15, 1, d_d[0]:int) != 1) goto B_f;
  a[15]:ubyte;
  label B_f:
  g_a = a + 16;
  label B_a:
}

export function f() {
  nop
}

function f_h(a:int):int {
  var c:int_ptr;
  var b:ubyte_ptr;
  b = a;
  if (b & 3) {
    loop L_c {
      if (eqz(b[0])) goto B_a;
      b = b + 1;
      if (b & 3) continue L_c;
    }
  }
  loop L_d {
    c = b;
    b = c + 4;
    var d:int = c[0];
    if (eqz(((d ^ -1) & d - 16843009) & -2139062144)) continue L_d;
  }
  loop L_e {
    b = c;
    c = b + 1;
    if (b[0]) continue L_e;
  }
  label B_a:
  return b - a;
}

function f_i(a:int, b:int):int {
  var d:int;
  var e:int;
  var c:int;
  var g:int;
  var f:int;
  if (b >
      if (d = 1136[0]:int) {
        d
      } else {
        if (f_j()) goto B_a;
        1136[0]:int;
      } -
      (e = 1140[0]:int)) {
    return call_indirect(1120, a, b, d_d[0]:int)
  }
  if (d_f[1]:int < 0) {
    d = 0;
    goto B_d;
  }
  c = b;
  loop L_f {
    d = c;
    if (eqz(d)) {
      d = 0;
      goto B_d;
    }
    if ((a + (c = d - 1))[0]:ubyte != 10) continue L_f;
  }
  c = call_indirect(1120, a, d, d_d[0]:int);
  if (c < d) goto B_a;
  a = a + d;
  b = b - d;
  e = 1140[0]:int;
  label B_d:
  c = e;
  if (b >= 512) {
    a_c(c, a, b);
    goto B_h;
  }
  e = b + c;
  if (eqz((a ^ c) & 3)) {
    if (eqz(c & 3)) goto B_l;
    if (eqz(b)) goto B_l;
    loop L_m {
      c[0]:byte = a[0]:ubyte;
      a = a + 1;
      c = c + 1;
      if (eqz(c & 3)) goto B_l;
      if (c < e) continue L_m;
    }
    label B_l:
    f = e & -4;
    if (f < 64) goto B_n;
    if (c > (g = f + -64)) goto B_n;
    loop L_o {
      c[0]:int = a[0]:int;
      c[1]:int = a[1]:int;
      c[2]:int = a[2]:int;
      c[3]:int = a[3]:int;
      c[4]:int = a[4]:int;
      c[5]:int = a[5]:int;
      c[6]:int = a[6]:int;
      c[7]:int = a[7]:int;
      c[8]:int = a[8]:int;
      c[9]:int = a[9]:int;
      c[10]:int = a[10]:int;
      c[11]:int = a[11]:int;
      c[12]:int = a[12]:int;
      c[13]:int = a[13]:int;
      c[14]:int = a[14]:int;
      c[15]:int = a[15]:int;
      a = a - -64;
      c = c - -64;
      if (c <= g) continue L_o;
    }
    label B_n:
    if (c >= f) goto B_j;
    loop L_p {
      c[0]:int = a[0]:int;
      a = a + 4;
      c = c + 4;
      if (c < f) continue L_p;
    }
    goto B_j;
  }
  if (e < 4) goto B_j;
  if (c > (f = e - 4)) goto B_j;
  loop L_q {
    c[0]:byte = a[0]:ubyte;
    c[1]:byte = a[1]:ubyte;
    c[2]:byte = a[2]:ubyte;
    c[3]:byte = a[3]:ubyte;
    a = a + 4;
    c = c + 4;
    if (c <= f) continue L_q;
  }
  label B_j:
  if (c < e) {
    loop L_s {
      c[0]:byte = a[0]:ubyte;
      a = a + 1;
      c = c + 1;
      if (c != e) continue L_s;
    }
  }
  label B_h:
  1140[0]:int = 1140[0]:int + b;
  c = b + d;
  label B_a:
  return c;
}

function f_j():int {
  var a:int;
  1192[0]:int = (a = 1192[0]:int) - 1 | a;
  a = d_b[0]:int;
  if (a & 8) {
    d_b[0]:int = a | 32;
    return -1;
  }
  1124[0]:long@4 = 0L;
  1148[0]:int = (a = d_d[2]:int);
  1140[0]:int = a;
  1136[0]:int = a + d_d[3]:int;
  return 0;
}

function f_k(a:int, b:long, c:int):long {
  return 0L
}

function f_l(a:int):int {
  return 0
}

function f_m(a:int, b:{ a:int, b:int }, c:int):int {
  var e:{ a:int, b:int }
  var g:int;
  var i:int;
  var j:int;
  var d:int_ptr = g_a - 32;
  g_a = d;
  d[4] = (e = a[7]:int);
  var f:int = a[5]:int;
  d[7] = c;
  d[6] = b;
  d[5] = (b = f - e);
  f = b + c;
  var h:int = 2;
  a = {
        e = a_a(a[15]:int, b = d + 16, 2, d + 12);
        if (if (e) {
              2328[0]:int = e;
              -1;
            } else {
              0
            }) {
          e = b;
          goto B_d;
        }
        loop L_g {
          if (f == (g = d[3])) goto B_c;
          if (g < 0) {
            e = b;
            goto B_b;
          }
          e = b + ((j = g > (i = b.b)) << 3);
          e.a = (i = g - select_if(i, 0, j)) + e.a;
          b = b + select_if(12, 4, j);
          b.a = b.a - i;
          f = f - g;
          g = a_a(a[15]:int, b = e, h = h - j, d + 12);
          if (eqz(if (g) {
                    2328[0]:int = g;
                    -1;
                  } else {
                    0
                  })) continue L_g;
        }
        label B_d:
        if (f != -1) goto B_b;
        label B_c:
        a[7]:int = (b = a[11]:int);
        a[5]:int = b;
        a[4]:int = b + a[12]:int;
        c;
        goto B_a;
        label B_b:
        a[7]:int = 0;
        a[2]:long = 0L;
        a[0]:int = a[0]:int | 32;
        0;
        if (h == 2) goto B_a;
        c - e.b;
        label B_a:
      }
  g_a = d + 32;
  return a;
}

export function h(a:int, b:int):int {
  a_d(321, 1092, 0);
  return 0;
}

export function k(a:int_ptr) {
  var b:int_ptr;
  var d:int_ptr;
  var e:int_ptr;
  var h:int_ptr;
  var g:int_ptr;
  if (eqz(a)) goto B_a;
  var c:int = a - 8;
  var f:int_ptr = c + (a = (b = (a - 4)[0]:int) & -8);
  if (b & 1) goto B_b;
  if (eqz(b & 3)) goto B_a;
  c = c - (b = c[0]:int);
  if (c < 2348[0]:int) goto B_a;
  a = a + b;
  if (2352[0]:int != c) {
    if (b <= 255) {
      e = c[2]:int;
      e == ((b = b >> 3) << 3) + 2372;
      if (e == (d = c[3]:int)) {
        2332[0]:int = 2332[0]:int & -2 << b;
        goto B_b;
      }
      e[3] = d;
      d[2] = e;
      goto B_b;
    }
    g = c[6]:int;
    if (c != (b = c[3]:int)) {
      d = c[2]:int;
      d[3] = b;
      b[2] = d;
      goto B_f;
    }
    e = c + 20;
    d = e[0];
    if (d) goto B_h;
    e = c + 16;
    d = e[0];
    if (d) goto B_h;
    b = 0;
    goto B_f;
    label B_h:
    loop L_i {
      h = e;
      b = d;
      e = b + 20;
      d = e[0];
      if (d) continue L_i;
      e = b + 16;
      d = b[4];
      if (d) continue L_i;
    }
    h[0] = 0;
    label B_f:
    if (eqz(g)) goto B_b;
    e = c[7]:int;
    d = (e << 2) + 2636;
    if (d[0] == c) {
      d[0] = b;
      if (b) goto B_j;
      2336[0]:int = 2336[0]:int & -2 << e;
      goto B_b;
    }
    (g + select_if(16, 20, g[4] == c))[0]:int = b;
    if (eqz(b)) goto B_b;
    label B_j:
    b[6] = g;
    d = c[4]:int;
    if (d) {
      b[4] = d;
      d[6] = b;
    }
    d = c[5]:int;
    if (eqz(d)) goto B_b;
    b[5] = d;
    d[6] = b;
    goto B_b;
  }
  b = f[1];
  if ((b & 3) != 3) goto B_b;
  2340[0]:int = a;
  f[1] = b & -2;
  c[1]:int = a | 1;
  (a + c)[0]:int = a;
  return ;
  label B_b:
  if (c >= f) goto B_a;
  b = f[1];
  if (eqz(b & 1)) goto B_a;
  if (eqz(b & 2)) {
    if (2356[0]:int == f) {
      2356[0]:int = c;
      2344[0]:int = (a = 2344[0]:int + a);
      c[1]:int = a | 1;
      if (c != 2352[0]:int) goto B_a;
      2340[0]:int = 0;
      2352[0]:int = 0;
      return ;
    }
    if (2352[0]:int == f) {
      2352[0]:int = c;
      2340[0]:int = (a = 2340[0]:int + a);
      c[1]:int = a | 1;
      (a + c)[0]:int = a;
      return ;
    }
    a = (b & -8) + a;
    if (b <= 255) {
      e = f[2];
      e == ((b = b >> 3) << 3) + 2372;
      if (e == (d = f[3])) {
        2332[0]:int = 2332[0]:int & -2 << b;
        goto B_q;
      }
      e[3] = d;
      d[2] = e;
      goto B_q;
    }
    g = f[6];
    if (f != (b = f[3])) {
      d = f[2];
      d < 2348[0]:int;
      d[3] = b;
      b[2] = d;
      goto B_t;
    }
    e = f + 20;
    d = e[0];
    if (d) goto B_v;
    e = f + 16;
    d = e[0];
    if (d) goto B_v;
    b = 0;
    goto B_t;
    label B_v:
    loop L_w {
      h = e;
      b = d;
      e = b + 20;
      d = e[0];
      if (d) continue L_w;
      e = b + 16;
      d = b[4];
      if (d) continue L_w;
    }
    h[0] = 0;
    label B_t:
    if (eqz(g)) goto B_q;
    e = f[7];
    d = (e << 2) + 2636;
    if (d[0] == f) {
      d[0] = b;
      if (b) goto B_x;
      2336[0]:int = 2336[0]:int & -2 << e;
      goto B_q;
    }
    (g + select_if(16, 20, g[4] == f))[0]:int = b;
    if (eqz(b)) goto B_q;
    label B_x:
    b[6] = g;
    d = f[4];
    if (d) {
      b[4] = d;
      d[6] = b;
    }
    d = f[5];
    if (eqz(d)) goto B_q;
    b[5] = d;
    d[6] = b;
    label B_q:
    c[1]:int = a | 1;
    (a + c)[0]:int = a;
    if (c != 2352[0]:int) goto B_m;
    2340[0]:int = a;
    return ;
  }
  f[1] = b & -2;
  c[1]:int = a | 1;
  (a + c)[0]:int = a;
  label B_m:
  if (a <= 255) {
    b = (a & -8) + 2372;
    a = {
          d = 2332[0]:int;
          if (eqz(d & (a = 1 << (a >> 3)))) {
            2332[0]:int = a | d;
            b;
            goto B_ba;
          }
          b[2];
          label B_ba:
        }
    b[2] = c;
    a[3] = c;
    c[3]:int = b;
    c[2]:int = a;
    return ;
  }
  e = 31;
  if (a <= 16777215) {
    b = a >> 8;
    b = b << (e = b + 1048320 >> 16 & 8);
    b = b << (d = b + 520192 >> 16 & 4);
    b = ((b << (b = b + 245760 >> 16 & 2)) >> 15) - ((d | e) | b);
    e = (b << 1 | (a >> b + 21 & 1)) + 28;
  }
  c[7]:int = e;
  c[4]:long@4 = 0L;
  h = (e << 2) + 2636;
  d = 2336[0]:int;
  if (eqz(d & (b = 1 << e))) {
    2336[0]:int = b | d;
    h[0] = c;
    c[6]:int = h;
    goto B_ga;
  }
  e = a << select_if(0, 25 - (e >> 1), e == 31);
  b = h[0];
  loop L_ia {
    d = b;
    if ((d[1] & -8) == a) goto B_fa;
    b = e >> 29;
    e = e << 1;
    h = d + (b & 4);
    b = (h + 16)[0]:int;
    if (b) continue L_ia;
  }
  h[4] = c;
  c[6]:int = d;
  label B_ga:
  c[3]:int = c;
  c[2]:int = c;
  goto B_ea;
  label B_fa:
  a = d[2];
  a[3] = c;
  d[2] = c;
  c[6]:int = 0;
  c[3]:int = d;
  c[2]:int = a;
  label B_ea:
  2364[0]:int = select_if(a = 2364[0]:int - 1, -1, a);
  label B_a:
}

export function j(a:int):int {
  var g:int;
  var d:{ a:int, b:int, c:int, d:int, e:int, f:int, g:int, h:int }
  var c:int;
  var h:int_ptr;
  var e:int;
  var i:int_ptr;
  var b:int;
  var f:{ a:int, b:int, c:int, d:int, e:int, f:int, g:int, h:int }
  var j:int_ptr;
  var k:int;
  var l:int = g_a - 16;
  g_a = l;
  if (a <= 244) {
    f = 2332[0]:int;
    b = f >> (a = (g = select_if(16, a + 11 & -8, a < 11)) >> 3);
    if (b & 3) {
      c = ((b ^ -1) & 1) + a;
      b = c << 3;
      a = b + 2372;
      if (a == (d = (b = (b + 2380)[0]:int)[2]:int)) {
        2332[0]:int = f & -2 << c;
        goto B_n;
      }
      d.d = a;
      a[2]:int = d;
      label B_n:
      a = b + 8;
      b[1]:int = (c = c << 3) | 3;
      b = b + c;
      b[1]:int = b[1]:int | 1;
      goto B_a;
    }
    if (g <= (h = 2340[0]:int)) goto B_k;
    if (b) {
      c = 2 << a;
      a = (c | 0 - c) & b << a;
      a = (a & 0 - a) - 1;
      b = a >> (a = a >> 12 & 16);
      c = b >> 5 & 8;
      b = 
        ((((c | a) | (b = (a = b >> c) >> 2 & 4)) | (b = (a = a >> b) >> 1 & 2)) |
         (b = (a = a >> b) >> 1 & 1)) +
        (a >> b);
      a = b << 3;
      c = a + 2372;
      if (c == (d = (a = (a + 2380)[0]:int)[2]:int)) {
        2332[0]:int = (f = f & -2 << b);
        goto B_q;
      }
      d.d = c;
      c[2]:int = d;
      label B_q:
      a[1]:int = g | 3;
      i = a + g;
      i[1] = (d = (b = b << 3) - g) | 1;
      (a + b)[0]:int = d;
      if (h) {
        b = (h & -8) + 2372;
        c = 2352[0]:int;
        e = {
              if (eqz(f & (e = 1 << (h >> 3)))) {
                2332[0]:int = e | f;
                b;
                goto B_t;
              }
              b[2]:int;
              label B_t:
            }
        b[2]:int = c;
        e[3]:int = c;
        c[3]:int = b;
        c[2]:int = e;
      }
      a = a + 8;
      2352[0]:int = i;
      2340[0]:int = d;
      goto B_a;
    }
    k = 2336[0]:int;
    if (eqz(k)) goto B_k;
    a = (k & 0 - k) - 1;
    b = a >> (a = a >> 12 & 16);
    c = b >> 5 & 8;
    c = 
      ((((((c | a) | (b = (a = b >> c) >> 2 & 4)) | (b = (a = a >> b) >> 1 & 2)) |
         (b = (a = a >> b) >> 1 & 1)) +
        (a >> b) <<
        2) +
       2636)[0]:int;
    e = (c[1]:int & -8) - g;
    b = c;
    loop L_v {
      a = b[4]:int;
      if (eqz(a)) {
        a = b[5]:int;
        if (eqz(a)) goto B_w;
      }
      b = (a[1]:int & -8) - g;
      e = select_if(b, e, b = b < e);
      c = select_if(a, c, b);
      b = a;
      continue L_v;
      label B_w:
    }
    j = c[6]:int;
    if (c != (d = c[3]:int)) {
      a = c[2]:int;
      a < 2348[0]:int;
      a[3]:int = d;
      d.c = a;
      goto B_b;
    }
    b = c + 20;
    a = b[0]:int;
    if (eqz(a)) {
      a = c[4]:int;
      if (eqz(a)) goto B_j;
      b = c + 16;
    }
    loop L_aa {
      i = b;
      d = a;
      b = d + 20;
      a = b[0]:int;
      if (a) continue L_aa;
      b = d + 16;
      a = d.e;
      if (a) continue L_aa;
    }
    i[0] = 0;
    goto B_b;
  }
  g = -1;
  if (a > -65) goto B_k;
  a = a + 11;
  g = a & -8;
  i = 2336[0]:int;
  if (eqz(i)) goto B_k;
  e = 0 - g;
  h = {
        0;
        if (g < 256) goto B_ea;
        31;
        if (g > 16777215) goto B_ea;
        a = a >> 8;
        b = a << (a = a + 1048320 >> 16 & 8);
        c = b << (b = b + 520192 >> 16 & 4);
        a = ((c << (c = c + 245760 >> 16 & 2)) >> 15) - ((a | b) | c);
        (a << 1 | (g >> a + 21 & 1)) + 28;
        label B_ea:
      }
  b = ((h << 2) + 2636)[0]:int;
  if (eqz(b)) {
    a = 0;
    goto B_da;
  }
  a = 0;
  c = g << select_if(0, 25 - (h >> 1), h == 31);
  loop L_ga {
    f = (b[1]:int & -8) - g;
    if (f >= e) goto B_ha;
    d = b;
    e = f;
    if (e) goto B_ha;
    e = 0;
    a = b;
    goto B_ca;
    label B_ha:
    a = 
      select_if(
        select_if(a, f = b[5]:int, f == (b = (b + (c >> 29 & 4))[4]:int)),
        a,
        f);
    c = c << 1;
    if (b) continue L_ga;
  }
  label B_da:
  if (eqz(a | d)) {
    d = 0;
    a = 2 << h;
    a = (a | 0 - a) & i;
    if (eqz(a)) goto B_k;
    a = (a & 0 - a) - 1;
    b = a >> (a = a >> 12 & 16);
    c = b >> 5 & 8;
    a = 
      ((((((c | a) | (b = (a = b >> c) >> 2 & 4)) | (b = (a = a >> b) >> 1 & 2)) |
         (b = (a = a >> b) >> 1 & 1)) +
        (a >> b) <<
        2) +
       2636)[0]:int;
  }
  if (eqz(a)) goto B_ba;
  label B_ca:
  loop L_ja {
    c = (a[1]:int & -8) - g;
    b = c < e;
    e = select_if(c, e, b);
    d = select_if(a, d, b);
    b = a[4]:int;
    a = if (b) { b } else { a[5]:int }
    if (a) continue L_ja;
  }
  label B_ba:
  if (eqz(d)) goto B_k;
  if (e >= 2340[0]:int - g) goto B_k;
  h = d.g;
  if (d != (c = d.d)) {
    a = d.c;
    a < 2348[0]:int;
    a[3]:int = c;
    c[2]:int = a;
    goto B_c;
  }
  b = d + 20;
  a = b[0]:int;
  if (eqz(a)) {
    a = d.e;
    if (eqz(a)) goto B_i;
    b = d + 16;
  }
  loop L_na {
    f = b;
    c = a;
    b = c + 20;
    a = b[0]:int;
    if (a) continue L_na;
    b = c + 16;
    a = c[4]:int;
    if (a) continue L_na;
  }
  f.a = 0;
  goto B_c;
  label B_k:
  if (g <= (b = 2340[0]:int)) {
    a = 2352[0]:int;
    c = b - g;
    if (c >= 16) {
      2340[0]:int = c;
      2352[0]:int = (d = a + g);
      d.b = c | 1;
      (a + b)[0]:int = c;
      a[1]:int = g | 3;
      goto B_pa;
    }
    2352[0]:int = 0;
    2340[0]:int = 0;
    a[1]:int = b | 3;
    b = a + b;
    b[1]:int = b[1]:int | 1;
    label B_pa:
    a = a + 8;
    goto B_a;
  }
  if (g < (c = 2344[0]:int)) {
    2344[0]:int = (b = c - g);
    2356[0]:int = (c = (a = 2356[0]:int) + g);
    c[1]:int = b | 1;
    a[1]:int = g | 3;
    a = a + 8;
    goto B_a;
  }
  a = 0;
  e = g + 47;
  f = e +
      (b = {
             if (2804[0]:int) {
               2812[0]:int;
               goto B_sa;
             }
             2816[0]:long@4 = -1L;
             2808[0]:long@4 = 17592186048512L;
             2804[0]:int = (l + 12 & -16) ^ 1431655768;
             2824[0]:int = 0;
             2776[0]:int = 0;
             4096;
             label B_sa:
           });
  b = f & (i = 0 - b);
  if (b <= g) goto B_a;
  d = 2772[0]:int;
  if (d) {
    h = 2764[0]:int;
    j = h + b;
    if (j <= h) goto B_a;
    if (d < j) goto B_a;
  }
  if (2776[0]:ubyte & 4) goto B_f;
  d = 2356[0]:int;
  if (d) {
    a = 2780;
    loop L_ya {
      if (d >= (h = a[0]:int)) { if (h + a[1]:int > d) goto B_wa }
      a = a[2]:int;
      if (a) continue L_ya;
    }
  }
  c = f_e(0);
  if (c == -1) goto B_g;
  f = b;
  a = 2808[0]:int;
  d = a - 1;
  if (d & c) { f = b - c + (c + d & 0 - a) }
  if (f <= g) goto B_g;
  if (f > 2147483646) goto B_g;
  a = 2772[0]:int;
  if (a) {
    d = 2764[0]:int;
    i = d + f;
    if (i <= d) goto B_g;
    if (a < i) goto B_g;
  }
  a = f_e(f);
  if (a != c) goto B_va;
  goto B_e;
  label B_wa:
  f = f - c & i;
  if (f > 2147483646) goto B_g;
  c = f_e(f);
  if (c == a[0]:int + a[1]:int) goto B_h;
  a = c;
  label B_va:
  if (a == -1) goto B_cb;
  if (g + 48 <= f) goto B_cb;
  c = 2812[0]:int;
  c = c + e - f & 0 - c;
  if (c > 2147483646) {
    c = a;
    goto B_e;
  }
  if (f_e(c) != -1) {
    f = c + f;
    c = a;
    goto B_e;
  }
  f_e(0 - f);
  goto B_g;
  label B_cb:
  c = a;
  if (c != -1) goto B_e;
  goto B_g;
  label B_j:
  d = 0;
  goto B_b;
  label B_i:
  c = 0;
  goto B_c;
  label B_h:
  if (c != -1) goto B_e;
  label B_g:
  2776[0]:int = 2776[0]:int | 4;
  label B_f:
  if (b > 2147483646) goto B_d;
  c = f_e(b);
  a = f_e(0);
  if (c == -1) goto B_d;
  if (a == -1) goto B_d;
  if (a <= c) goto B_d;
  f = a - c;
  if (f <= g + 40) goto B_d;
  label B_e:
  2764[0]:int = (a = 2764[0]:int + f);
  if (2768[0]:int < a) { 2768[0]:int = a }
  e = 2356[0]:int;
  if (e) {
    a = 2780;
    loop L_kb {
      if (c == (b = a[0]:int) + (d = a[1]:int)) goto B_ib;
      a = a[2]:int;
      if (a) continue L_kb;
    }
    goto B_hb;
  }
  a = 2348[0]:int;
  if (eqz(select_if(a, 0, a <= c))) { 2348[0]:int = c }
  a = 0;
  2784[0]:int = f;
  2780[0]:int = c;
  2364[0]:int = -1;
  2368[0]:int = 2804[0]:int;
  2792[0]:int = 0;
  loop L_mb {
    b = a << 3;
    (b + 2380)[0]:int = (d = b + 2372);
    (b + 2384)[0]:int = d;
    a = a + 1;
    if (a != 32) continue L_mb;
  }
  2344[0]:int =
    (d = (a = f - 40) - (b = select_if(-8 - c & 7, 0, c + 8 & 7)));
  2356[0]:int = (b = b + c);
  b[1]:int = d | 1;
  (a + c)[1]:int = 40;
  2360[0]:int = 2820[0]:int;
  goto B_gb;
  label B_ib:
  if (a[12]:ubyte & 8) goto B_hb;
  if (b > e) goto B_hb;
  if (c <= e) goto B_hb;
  a[1]:int = d + f;
  2356[0]:int = (b = e + (a = select_if(-8 - e & 7, 0, e + 8 & 7)));
  2344[0]:int = (a = (c = 2344[0]:int + f) - a);
  b[1]:int = a | 1;
  (c + e)[1]:int = 40;
  2360[0]:int = 2820[0]:int;
  goto B_gb;
  label B_hb:
  if (2348[0]:int > c) { 2348[0]:int = c }
  b = c + f;
  a = 2780;
  loop L_ub {
    if (b != a[0]:int) {
      a = a[2]:int;
      if (a) continue L_ub;
      goto B_tb;
    }
  }
  if (eqz(a[12]:ubyte & 8)) goto B_sb;
  label B_tb:
  a = 2780;
  loop L_wb {
    if (e >= (b = a[0]:int)) {
      d = b + a[1]:int;
      if (d > e) goto B_rb;
    }
    a = a[2]:int;
    continue L_wb;
  }
  unreachable;
  label B_sb:
  a[0]:int = c;
  a[1]:int = a[1]:int + f;
  h = c + select_if(-8 - c & 7, 0, c + 8 & 7);
  h[1] = g | 3;
  f = b + select_if(-8 - b & 7, 0, b + 8 & 7);
  a = f - (g = g + h);
  if (e == f) {
    2356[0]:int = g;
    2344[0]:int = (a = 2344[0]:int + a);
    g[1]:int = a | 1;
    goto B_pb;
  }
  if (2352[0]:int == f) {
    2352[0]:int = g;
    2340[0]:int = (a = 2340[0]:int + a);
    g[1]:int = a | 1;
    (a + g)[0]:int = a;
    goto B_pb;
  }
  e = f.b;
  if ((e & 3) == 1) {
    j = e & -8;
    if (e <= 255) {
      b = f.c;
      b == ((d = e >> 3) << 3) + 2372;
      if (b == (c = f.d)) {
        2332[0]:int = 2332[0]:int & -2 << d;
        goto B_bc;
      }
      b[3]:int = c;
      c[2]:int = b;
      goto B_bc;
    }
    i = f.g;
    if (f != (c = f.d)) {
      b = f.c;
      b[3]:int = c;
      c[2]:int = b;
      goto B_ec;
    }
    e = f + 20;
    b = e[0]:int;
    if (b) goto B_gc;
    e = f + 16;
    b = e[0]:int;
    if (b) goto B_gc;
    c = 0;
    goto B_ec;
    label B_gc:
    loop L_hc {
      d = e;
      c = b;
      e = c + 20;
      b = e[0]:int;
      if (b) continue L_hc;
      e = c + 16;
      b = c[4]:int;
      if (b) continue L_hc;
    }
    d.a = 0;
    label B_ec:
    if (eqz(i)) goto B_bc;
    b = f.h;
    d = (b << 2) + 2636;
    if (d.a == f) {
      d.a = c;
      if (c) goto B_ic;
      2336[0]:int = 2336[0]:int & -2 << b;
      goto B_bc;
    }
    (i + select_if(16, 20, i[4] == f))[0]:int = c;
    if (eqz(c)) goto B_bc;
    label B_ic:
    c[6]:int = i;
    b = f.e;
    if (b) {
      c[4]:int = b;
      b[6]:int = c;
    }
    b = f.f;
    if (eqz(b)) goto B_bc;
    c[5]:int = b;
    b[6]:int = c;
    label B_bc:
    f = f + j;
    e = f.b;
    a = a + j;
  }
  f.b = e & -2;
  g[1]:int = a | 1;
  (a + g)[0]:int = a;
  if (a <= 255) {
    b = (a & -8) + 2372;
    a = {
          c = 2332[0]:int;
          if (eqz(c & (a = 1 << (a >> 3)))) {
            2332[0]:int = a | c;
            b;
            goto B_mc;
          }
          b[2]:int;
          label B_mc:
        }
    b[2]:int = g;
    a[3]:int = g;
    g[3]:int = b;
    g[2]:int = a;
    goto B_pb;
  }
  e = 31;
  if (a <= 16777215) {
    b = a >> 8;
    c = b << (b = b + 1048320 >> 16 & 8);
    d = c << (c = c + 520192 >> 16 & 4);
    b = ((d << (d = d + 245760 >> 16 & 2)) >> 15) - ((b | c) | d);
    e = (b << 1 | (a >> b + 21 & 1)) + 28;
  }
  g[7]:int = e;
  g[4]:long@4 = 0L;
  b = (e << 2) + 2636;
  c = 2336[0]:int;
  if (eqz(c & (d = 1 << e))) {
    2336[0]:int = c | d;
    b[0]:int = g;
    goto B_pc;
  }
  e = a << select_if(0, 25 - (e >> 1), e == 31);
  c = b[0]:int;
  loop L_rc {
    b = c;
    if ((b[1]:int & -8) == a) goto B_qb;
    c = e >> 29;
    e = e << 1;
    d = b + (c & 4);
    c = d.e;
    if (c) continue L_rc;
  }
  d.e = g;
  label B_pc:
  g[6]:int = b;
  g[3]:int = g;
  g[2]:int = g;
  goto B_pb;
  label B_rb:
  2344[0]:int =
    (i = (a = f - 40) - (b = select_if(-8 - c & 7, 0, c + 8 & 7)));
  2356[0]:int = (b = b + c);
  b[1]:int = i | 1;
  (a + c)[1]:int = 40;
  2360[0]:int = 2820[0]:int;
  b = select_if(e,
                a = d + select_if(39 - d & 7, 0, d - 39 & 7) - 47,
                a < e + 16);
  b[1]:int = 27;
  b[4]:long@4 = 2788[0]:long@4;
  b[2]:long@4 = 2780[0]:long@4;
  2788[0]:int = b + 8;
  2784[0]:int = f;
  2780[0]:int = c;
  2792[0]:int = 0;
  a = b + 24;
  loop L_sc {
    a[1]:int = 7;
    c = a + 8;
    a = a + 4;
    if (c < d) continue L_sc;
  }
  if (b == e) goto B_gb;
  b[1]:int = b[1]:int & -2;
  e[1]:int = (c = b - e) | 1;
  b[0]:int = c;
  if (c <= 255) {
    a = (c & -8) + 2372;
    b = {
          b = 2332[0]:int;
          if (eqz(b & (c = 1 << (c >> 3)))) {
            2332[0]:int = b | c;
            a;
            goto B_uc;
          }
          a[2]:int;
          label B_uc:
        }
    a[2]:int = e;
    b[3]:int = e;
    e[3]:int = a;
    e[2]:int = b;
    goto B_gb;
  }
  a = 31;
  if (c <= 16777215) {
    a = c >> 8;
    b = a << (a = a + 1048320 >> 16 & 8);
    d = b << (b = b + 520192 >> 16 & 4);
    a = ((d << (d = d + 245760 >> 16 & 2)) >> 15) - ((a | b) | d);
    a = (a << 1 | (c >> a + 21 & 1)) + 28;
  }
  e[7]:int = a;
  e[4]:long@4 = 0L;
  b = (a << 2) + 2636;
  d = 2336[0]:int;
  if (eqz(d & (f = 1 << a))) {
    2336[0]:int = d | f;
    b[0]:int = e;
    goto B_xc;
  }
  a = c << select_if(0, 25 - (a >> 1), a == 31);
  d = b[0]:int;
  loop L_zc {
    b = d;
    if ((b[1]:int & -8) == c) goto B_ob;
    d = a >> 29;
    a = a << 1;
    f = b + (d & 4);
    d = f.e;
    if (d) continue L_zc;
  }
  f.e = e;
  label B_xc:
  e[6]:int = b;
  e[3]:int = e;
  e[2]:int = e;
  goto B_gb;
  label B_qb:
  a = b[2]:int;
  a[3]:int = g;
  b[2]:int = g;
  g[6]:int = 0;
  g[3]:int = b;
  g[2]:int = a;
  label B_pb:
  a = h + 8;
  goto B_a;
  label B_ob:
  a = b[2]:int;
  a[3]:int = e;
  b[2]:int = e;
  e[6]:int = 0;
  e[3]:int = b;
  e[2]:int = a;
  label B_gb:
  a = 2344[0]:int;
  if (a <= g) goto B_d;
  2344[0]:int = (b = a - g);
  2356[0]:int = (c = (a = 2356[0]:int) + g);
  c[1]:int = b | 1;
  a[1]:int = g | 3;
  a = a + 8;
  goto B_a;
  label B_d:
  2328[0]:int = 48;
  a = 0;
  goto B_a;
  label B_c:
  if (eqz(h)) goto B_ad;
  a = d.h;
  b = (a << 2) + 2636;
  if (b[0]:int == d) {
    b[0]:int = c;
    if (c) goto B_bd;
    2336[0]:int = (i = i & -2 << a);
    goto B_ad;
  }
  (h + select_if(16, 20, h[4] == d))[0]:int = c;
  if (eqz(c)) goto B_ad;
  label B_bd:
  c[6]:int = h;
  a = d.e;
  if (a) {
    c[4]:int = a;
    a[6]:int = c;
  }
  a = d.f;
  if (eqz(a)) goto B_ad;
  c[5]:int = a;
  a[6]:int = c;
  label B_ad:
  if (e <= 15) {
    d.b = (a = e + g) | 3;
    a = a + d;
    a[1]:int = a[1]:int | 1;
    goto B_ed;
  }
  d.b = g | 3;
  c = d + g;
  c[1]:int = e | 1;
  (c + e)[0]:int = e;
  if (e <= 255) {
    a = (e & -8) + 2372;
    b = {
          b = 2332[0]:int;
          if (eqz(b & (e = 1 << (e >> 3)))) {
            2332[0]:int = b | e;
            a;
            goto B_hd;
          }
          a[2]:int;
          label B_hd:
        }
    a[2]:int = c;
    b[3]:int = c;
    c[3]:int = a;
    c[2]:int = b;
    goto B_ed;
  }
  a = 31;
  if (e <= 16777215) {
    a = e >> 8;
    b = a << (a = a + 1048320 >> 16 & 8);
    f = b << (b = b + 520192 >> 16 & 4);
    a = ((f << (f = f + 245760 >> 16 & 2)) >> 15) - ((a | b) | f);
    a = (a << 1 | (e >> a + 21 & 1)) + 28;
  }
  c[7]:int = a;
  c[4]:long@4 = 0L;
  b = (a << 2) + 2636;
  if (eqz(i & (f = 1 << a))) {
    2336[0]:int = f | i;
    b[0]:int = c;
    goto B_ld;
  }
  a = e << select_if(0, 25 - (a >> 1), a == 31);
  g = b[0]:int;
  loop L_nd {
    b = g;
    if ((b[1]:int & -8) == e) goto B_kd;
    f = a >> 29;
    a = a << 1;
    f = b + (f & 4);
    g = f.e;
    if (g) continue L_nd;
  }
  f.e = c;
  label B_ld:
  c[6]:int = b;
  c[3]:int = c;
  c[2]:int = c;
  goto B_ed;
  label B_kd:
  a = b[2]:int;
  a[3]:int = c;
  b[2]:int = c;
  c[6]:int = 0;
  c[3]:int = b;
  c[2]:int = a;
  label B_ed:
  a = d + 8;
  goto B_a;
  label B_b:
  if (eqz(j)) goto B_od;
  a = c[7]:int;
  b = (a << 2) + 2636;
  if (b[0]:int == c) {
    b[0]:int = d;
    if (d) goto B_pd;
    2336[0]:int = k & -2 << a;
    goto B_od;
  }
  (j + select_if(16, 20, j[4] == c))[0]:int = d;
  if (eqz(d)) goto B_od;
  label B_pd:
  d.g = j;
  a = c[4]:int;
  if (a) {
    d.e = a;
    a[6]:int = d;
  }
  a = c[5]:int;
  if (eqz(a)) goto B_od;
  d.f = a;
  a[6]:int = d;
  label B_od:
  if (e <= 15) {
    c[1]:int = (a = e + g) | 3;
    a = a + c;
    a[1]:int = a[1]:int | 1;
    goto B_sd;
  }
  c[1]:int = g | 3;
  d = c + g;
  d.b = e | 1;
  (d + e)[0]:int = e;
  if (h) {
    a = (h & -8) + 2372;
    b = 2352[0]:int;
    f = {
          g = 1 << (h >> 3);
          if (eqz(g & f)) {
            2332[0]:int = f | g;
            a;
            goto B_vd;
          }
          a[2]:int;
          label B_vd:
        }
    a[2]:int = b;
    f.d = b;
    b[3]:int = a;
    b[2]:int = f;
  }
  2352[0]:int = d;
  2340[0]:int = e;
  label B_sd:
  a = c + 8;
  label B_a:
  g_a = l + 16;
  return a;
}

export function n(a:int):int {
  a = g_a - a & -16;
  g_a = a;
  return a;
}

export function m(a:int) {
  g_a = a
}

export function l():int {
  return g_a
}

export function g(a:int):int {
  var b:long;
  var e:int;
  var c:{ a:long, b:byte, c:byte, d:long, e:byte, f:byte } = g_a - 32;
  g_a = c;
  var d:int = f_h(a);
  if (d % 3) goto B_a;
  if (d - 30 < -5) goto B_a;
  f_f(1024);
  d = 8;
  c.e = a[8]:ubyte;
  c.d = a[0]:long@1;
  c.b = a[17]:ubyte;
  c.a = a[9]:long@1;
  var f:int = a[18]:byte;
  var g:int = a[19]:byte;
  var h:int = a[20]:byte;
  var i:int = a[21]:byte;
  var j:int = a[22]:byte;
  var k:int = a[23]:byte;
  var l:int = a[24]:byte;
  var m:int = a[25]:byte;
  var n:int = a[26]:byte;
  c.f = 0;
  c.c = 0;
  loop L_b {
    if ((1089 - (a = d))[0]:ubyte != (c + 16 + a)[0]:ubyte) goto B_a;
    d = a - 1;
    if (a) continue L_b;
  }
  f_f(1072);
  a = 0;
  loop L_c {
    if (
      (a + 1044)[0]:ubyte != ((a + c)[0]:ubyte ^ (a + 1081)[0]:ubyte)) goto B_a;
    a = a + 1;
    if (a != 9) continue L_c;
  }
  f_f(1063);
  1280[0]:long = i64_extend_i32_u(c.b - 1);
  1280[0]:long = (b = 1280[0]:long * 6364136223846793005L + 1L);
  var o:int = i32_wrap_i64(b >> 33L) % 50 ^ f;
  1280[0]:long = (b = 1280[0]:long * 6364136223846793005L + 1L);
  var p:int = o + (i32_wrap_i64(b >> 33L) % 50 ^ g);
  1280[0]:long = (b = 1280[0]:long * 6364136223846793005L + 1L);
  var q:int = p + (i32_wrap_i64(b >> 33L) % 50 ^ h);
  1280[0]:long = (b = 1280[0]:long * 6364136223846793005L + 1L);
  var r:int = q + (i32_wrap_i64(b >> 33L) % 50 ^ i);
  1280[0]:long = (b = 1280[0]:long * 6364136223846793005L + 1L);
  var s:int = r + (i32_wrap_i64(b >> 33L) % 50 ^ j);
  1280[0]:long = (b = 1280[0]:long * 6364136223846793005L + 1L);
  var t:int = s + (i32_wrap_i64(b >> 33L) % 50 ^ k);
  1280[0]:long = (b = 1280[0]:long * 6364136223846793005L + 1L);
  var u:int = t + (i32_wrap_i64(b >> 33L) % 50 ^ l);
  1280[0]:long = (b = 1280[0]:long * 6364136223846793005L + 1L);
  var v:int = u + (i32_wrap_i64(b >> 33L) % 50 ^ m);
  1280[0]:long = (b = 1280[0]:long * 6364136223846793005L + 1L);
  a = v + (i32_wrap_i64(b >> 33L) % 50 ^ n);
  if (a != 787) {
    e = 788 - a;
    goto B_a;
  }
  f_f(1054);
  e = 1;
  label B_a:
  g_a = c + 32;
  return e;
}

