#!/usr/bin/env python3

from string import ascii_uppercase as auc
from string import ascii_lowercase as alc

import numpy as np

def test(guess):
    rng = {
        0: np.int32(49),
        1: np.int32(16),
        2: np.int32(42),
        3: np.int32(9),
        4: np.int32(9),
        5: np.int32(3),
        6: np.int32(10),
        7: np.int32(6),
        8: np.int32(28)
    }

    #print(f"{base}{guess}")
    running_sum = 0
    reverse_sum = {
        0: 0,
        1: 0,
        2: 0,
        3: 0,
        4: 0,
        5: 0,
        6: 0,
        7: 0,
        8: 787
    }
    for i in [7, 6, 5, 4, 3, 2, 1, 0]:
        asc = ord(guess[i])
        xor = np.bitwise_xor(rng[i], np.int32(asc))
        reverse_sum[i] = reverse_sum[i + 1] - xor

    for i in range(0, 9):
        asc = ord(guess[i])
        xor = np.bitwise_xor(rng[i], np.int32(asc))
        running_sum += xor
        #print(f"{np.binary_repr(rng[i], width=32)} ({rng[i]:02}) xor {guess[i]} ({asc:03}) = {np.binary_repr(xor, width=32)} = {xor:03} | {running_sum:03} | {reverse_sum[i]}") 

    if running_sum == 787:
        print(f"{base}{guess}")

base = "RH_CTF{W3lc0me_A55"
guess = "xxxxxxxx}"

numeric = []
for i in range(0, 10):
    numeric.extend([chr(48 + i)])

alphanumeric = list(auc) + list(alc) + ['_', '!', '@', '#', '$', '%', '^', '&', '*'] + numeric

for i in alphanumeric:
    for j in alphanumeric:
        for k in alphanumeric:
            for l in alphanumeric:
                for m in alphanumeric:
                    for n in alphanumeric:
                        for o in alphanumeric:
                            for p in alphanumeric:
                                guess = list(guess)
                                guess[7] = i
                                guess[6] = j
                                guess[5] = k
                                guess[4] = l
                                guess[3] = m
                                guess[2] = n
                                guess[1] = o
                                guess[0] = p
                                test("".join(guess))
