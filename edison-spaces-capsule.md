#  Edison Space's capsule (350 points)

Edison Space made a bold choice: use only JavaScript technologies for its Wyvern
capsule. Now in space, their capsule is connected to Edisson Space's network. We
suspect the oldest rubber ducky was stolen and put in the capsule, can you
confirm this?

https://login-capsule-edison-space.ctf.nofizz.buzz 

# Introduction: Web login page

A javascript based login authentication means all of the password processing is
done client-side. This should be a piece of cake.

```javascript
function submitLogin(t){const e=Module.cwrap("login","number",["string"])(t[0].value);t[0].classList.toggle("invalid",1!==e),t[0].classList.toggle("valid",1===e),1===e&&(t[0].type="text",t[0].readOnly=!0,alert("Good password! use it to validate the challenge."))}
```

```javascript
wasmBinaryFile="login.wasm";
```

A quick investigation of the files shows a login.js and a login.wasm. And that
the login code steps is being performed in the wasm. Not so easy to solve as
this was our first real encounter with wasm.

We found no passwords visible in the login.js nor any simple authentication
functions. The login.js looks obfuscated with misleading code paths. The
login.wasm is binary code so it is not human readable.

## Part 1a. Running strings on login.wasm to get a starting point

Quickly getting a feel for the files, we downloaded the website files and ran
`strings` on login.wasm. From there we found a starting point: "RH_CTF{W3" is
stored there in reverse.

![01_strings.png](/images/01_strings.png "01_strings.png")

This told us that the wasm file is holding our secrets.

## Part 1b. Debugging through the javascript/wasm swamp to find the password length

We disassembled the wasm binary with wasm2wat (a human readable format of the
wasm) and we found some of the data buffers in their raw form.

```
data d_passedlengthcheck_4K1gpass3O(offset: 1024) =
  "passed length check\00_4K+1\1c\1e}g\00pass3 OK\00pass2 OK\00pass1 OK\00"
  "3W{FTC_HR'\00Login module loaded\0a";
```

From this we made the assumption that there are four checks going on in the
password checker. The first is a password length check, followed by 3 other
checks.

Knowing that there is a length check, we repeatedly entered a random set of
characters into the login prompt and lengthened the random string on each
attempt until the console prints out the debug message “passed length check”.

![02_passed_length_check.png](/images/02_passed_length_check.png "02_passed_length_check.png")

On success, we now have the length of the password in total: 27 chars. Since we
know the last character will end with “}” we have found another character of
our password.

We are up to:

```
RH_CTF{W3xxxxxxxxxxxxxxxxx}
```

When we entered this into the password input field, we also saw “pass1 OK”
printed in the web browser console so we knew that the first pass check probably
checked for the string we found in the wasm. So now we are onto part 2.

## Part 2: Breakpointing loops to find the second part of the password

Chrome lets us see the WASM in WAT format which makes for easy debugging. We
can also apply some breakpoints and peek at the stack/heap values (which was
not possible in Firefox!).

While stepping through the WASM, we found an interesting loop occurring just
before the code returned to the javascript call function.

![03_wasmloop.png](/images/03_wasmloop.png "03_wasmloop.png")

Rather than applying any braincells to understanding the algorithm, we simply
set a breakpoint on 0x024bf to observe how often the loop runs and this told us
how many of the characters in the second part of the password were correct.

We created a small python script to output the password variations for the
character I am hunting (considering alphanumeric + underscores).

```python
#!/usr/bin/env python3

from string import ascii_uppercase as auc
from string import ascii_lowercase as alc

loop = list(range(0, 10)) + list(auc) + list(alc) + ['_']
for i in loop:
    print("RH_CTF{W3%xxxxxxxxxxxxxxxx}" % i)
```

We then manually pecked these into the keyboard. At first we failed to hit the
breakpoint but once we replaced the first character with a lowercase L, it finally
hit.

```
RH_CTF{W3lxxxxxxxxxxxxxxxx}
```

We could see from the stack that the number from the add instruction in 0x24bc
is 1. This gave us a count of the number of loops run and was an easy way to count
the number of successful characters entered (as a wrong character causes the loop
to exit).

_(At first we did this in firefox and had to simply human count the number of times
the breakpoint hit because the stack was not observable)._

![04_wasmstackcount.png](/images/04_wasmstackcount.png "04_wasmstackcount.png")

Eventually we found ourselves having entered:

```
RH_CTF{W3lc0me_A55xxxxxxxx}
```

The stack counter reached nine and the loop exited cleanly.

Now we are onto part 3, the final part.

## Part 3: Extracting statically seeded rng xor values to find the third part of the password

Stepping through the code after the loop from part 2 showed it enters a block
of code that is a linear set of xor operations followed by a final checksum
check.

![05_wasmxor.png](/images/05_wasmxor.png "05_wasmxor.png")

To tackle this, we broke down the WAT into annotated code to better understand
what the algorithm is doing. The code is per password character generating a
random number using a Linear Congruential Generator (LCG) and using that number
to xor the ascii character value to get another value. The total sum of these
values is compared to 787 and if they equal, is considered a valid password.

![10_787.png](/images/10_787.png "10_787.png")

The annotation files:

[wasm_annotations.orig.txt](/assets/wasm_annotations.orig.txt) has the original
WAT annotated.

[wasm_annotations.txt](/assets/wasm_annotations.txt) has the wasm slightly shuffled
to clear up some obfuscation.

_Note that we made a mistake here assuming that it’s not trivial to check per
password character but apparently that should've been possible based on the flip
of the sign bit as an indicator. This write up continues with our brute force
approach to solve this._

At first we attempted to reverse the LCG from the final 787 value but since we
had two unknowns (the character and the seed) we were stumped there. Then while
stepping through the code and observing the stack, we found that the rng values
being calculated seemed to be static.

The code seems to load the last password character from part 2 to use as a seed
for the LCG so we have a static seed. On first finding this, we began implementing
the LCG algorithm in Python to be able to get the intermediate xor values but
quickly realized instead that we can simply step through the WAT and at certain
breakpoints just extract the calculated rng values for each character. (This was
the point we learned Chrome has a much better debugger for WASM than Firefox).

![06_wasmlcgvals.png](/images/06_wasmlcgvals.png "06_wasmlcgvals.png")

Here we see at the first character, the xor will be performed between stack values:
49 and 120 (120 being the character x in our password). Therefore the xor value is
49. Repeating this for the entirety of the code showed the following values for the
xor operations:

```
49, 16, 42, 9, 9, 3, 10, 6, 28
```

Now we can make a small Python script to brute force this for us. We can xor randomly
generated passwords until we find those with a checksum value of 787.

See [xor1.py](/assets/xor1.py) for the brute force script.

Straightaway we found an avalanche of answers, seemingly all random.

Eg. RH_CTF{W3lc0me_A55KxFAAAAA}

Trying one of these on the webpage though showed... success?

![07_success1.png](/images/07_success1.png "07_success1.png")
![08_success2.png](/images/08_success2.png "08_success2.png")
![09_success3.png](/images/09_success3.png "09_success3.png")

### Trying to narrow down our search

We modified our brute forcer to try to narrow our search down. Since we see the
password so far ended with A55 which is l33tc0de for "ass", we assume maybe
this means asset. Since there's been a play on the word "duck" as well so far
in the challenges, we try to find variations of these two words.

eg. `asset_duck0`

See [xor2.py](/assets/xor2.py) for the narrowed search program.

Despite narrowing down the search, we still had dozens of results.

### Admitting defeat...

We checked in with the CTF organisers but received no confirmation whether
this is expected or not. Meanwhile we saw another team managed to solve the
puzzle so we assume we're simply missing some clue that would narrow down the
password for us.

We accept defeat and go to sleep.

### Oops: We broke the challenge

The next day, the CTF organisers check in on us again and confirm it's not
expected behaviour! We have broken the login page beyond simply finding the
password and instead broken the login checker algorithm.

The challenge website is modified to accept all of our answers and we close up
this puzzle with a randomly picked password from our thousands of answers found.

This goes to show: checksum collisions are a real thing.

